﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ab_Utility
{
    interface ICorpseTickable
    {
        /// <summary>
        /// Only called on corpses
        /// </summary>
        void CorpseTickRare();
    }
}
