﻿using HarmonyLib;
using RimWorld;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Verse;

namespace Ab_Utility
{
	[StaticConstructorOnStartup]
	public static class Startup 
	{
		static Startup()
		{
			Harmony harmony = new Harmony("AbUtility");
			Harmony.DEBUG = Ab_Mod.settings.Harmony_Logging;//never release a build with this set to true.
			harmony.PatchAll(Assembly.GetExecutingAssembly());
			ManualPatches(harmony);
		}
		private static void ManualPatches(Harmony harmony)
		{
		}

		static MethodInfo TopButtonsLocalMethodFinder(Type type)
		{
			return AccessTools.FirstMethod(type, m => m.Name == "<DoTopStack>b__15");
		}
	}
}