﻿using AlienRace.ExtendedGraphics;
using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using UnityEngine;
using Verse;
using static AlienRace.AlienPartGenerator;

namespace Ab_Utility
{
	//ToDo : Debug settings mod and lots of log.warrnings with it.
	public class KeyedSetting : IExposable
	{
		public bool Value;
		private string Label;
		private string Tooltip;

		/// <summary>
		/// Do not use this Constructor!!! Use <see cref="KeyedSetting.KeyedSetting(bool, string, string)"/>
		/// </summary>
		/// <exception cref="Exception"></exception>
		public KeyedSetting()
		{
			if (Scribe.mode == LoadSaveMode.Inactive)
			{
				throw new Exception("Constructor called outside of scribing.");
			}
		}

		public KeyedSetting(bool defaultValue, string label, string tooltip = null)
		{
			Value = defaultValue;
			Label = label;
			Tooltip = tooltip;
		}

		public void ExposeData()
		{
			Scribe_Values.Look(ref Value, "Value");
			Scribe_Values.Look(ref Label, "Label");
			Scribe_Values.Look(ref Tooltip, "Tooltip");
		}

		public void DrawSetting(Listing_Standard list)
		{
			list.CheckboxLabeled(Label.Translate(), ref Value, Tooltip.Translate());
		}
	}
	public class Settings : ModSettings
	{
		public bool DevAssets = false;
		public bool HAR_Logging = true;
		public bool HAR_ForceLogging = false;
		public bool Harmony_Logging = false;

		//features
		public bool RoyalFavorImpact = true;

		//content
		public bool CustomStartScenarios = true;

		//KeyedSettings
		private Dictionary<string, KeyedSetting> KeyedSettings;
		//features
		public KeyedSetting EnableAlienRaceProstheticGraphics;
		public const string EnableAlienProstheticGraphicsKey = "Enable_AlienProstheticGraphics";

		public KeyedSetting EnableGenesForHairColor;
		public const string EnableGenesForHairColorKey = "Enable_GenesForHairColor";
		//content
		public KeyedSetting EnableProsthetics;
		public const string EnableProstheticsKey = "Enable_Prosthetics";

		public KeyedSetting EnableRoyalFavorImpactPatches;
		public string EnableRoyalFavorImpactPatchesKey = "Enable_FavorStatPatch";

		public KeyedSetting MonoColoredDemiFox;

		public Settings()
		{
			ValidateKeyedSettings();
		}

		public void ValidateKeyedSettings()
		{
			if (KeyedSettings == null)
			{
				KeyedSettings = new Dictionary<string, KeyedSetting>();
			}
			//features
			MonoColoredDemiFox = KeyedSettings.TryGetValue("dFoxAddonShader");
			if (MonoColoredDemiFox == null)
			{
				MonoColoredDemiFox = new KeyedSetting(false, "DemiFox_Addons_MonoColored", "DemiFox_Addons_MonoColored_Tip");
				KeyedSettings.Add("dFoxAddonShader", MonoColoredDemiFox);
			}

			EnableAlienRaceProstheticGraphics = KeyedSettings.TryGetValue(EnableAlienProstheticGraphicsKey);
			if (EnableAlienRaceProstheticGraphics == null)
			{
				EnableAlienRaceProstheticGraphics = new KeyedSetting(true, "Ab_Enable_AlienRaceProstheticGraphics", "Ab_Enable_AlienRaceProstheticGraphics_Tip");
				KeyedSettings.Add(EnableAlienProstheticGraphicsKey, EnableAlienRaceProstheticGraphics);
			}

			EnableGenesForHairColor = KeyedSettings.TryGetValue(EnableGenesForHairColorKey);
			if (EnableGenesForHairColor == null)
			{
				EnableGenesForHairColor = new KeyedSetting(false, "Ab_Enable_GenesForHairColor", "Ab_Enable_GenesForHairColor_Tip");
				KeyedSettings.Add(EnableGenesForHairColorKey, EnableGenesForHairColor);
			}

			//content
			EnableProsthetics = KeyedSettings.TryGetValue(EnableProstheticsKey);
			if(EnableProsthetics == null)
			{
				EnableProsthetics = new KeyedSetting(true, "Ab_Enable_Prosthetics", "Ab_Enable_Prosthetics_Tip");
				KeyedSettings.Add(EnableProstheticsKey, EnableProsthetics);
			}

			EnableRoyalFavorImpactPatches = KeyedSettings.TryGetValue(EnableRoyalFavorImpactPatchesKey);
			if (EnableRoyalFavorImpactPatches == null)
			{
				EnableRoyalFavorImpactPatches = new KeyedSetting(true, "Ab_Enable_RoyalFavorStatPatch", "Ab_Enable_RoyalFavorStatPatch_Tip");
				KeyedSettings.Add(EnableRoyalFavorImpactPatchesKey, EnableRoyalFavorImpactPatches);
			}
		}

		public bool IsKeyedSettingEnabled(string key)
		{
			KeyedSetting setting = KeyedSettings.TryGetValue(key);
			if (setting == null)
			{
				return false;
			}
			return setting.Value;
		}

		public override void ExposeData()
		{
			Scribe_Values.Look(ref DevAssets, "DevAssets", defaultValue: false);
			Scribe_Values.Look(ref HAR_Logging, "HAR_Logging", defaultValue: true);
			Scribe_Values.Look(ref HAR_ForceLogging, "HAR_ForceLogging", defaultValue: false);
			Scribe_Values.Look(ref Harmony_Logging, "Harmony_Logging", defaultValue: false);

			Scribe_Values.Look(ref RoyalFavorImpact, "RoyalFavorImpact", defaultValue: true);

			Scribe_Values.Look(ref CustomStartScenarios, "CustomStartScenarios", defaultValue: true);

			Scribe_Collections.Look(ref KeyedSettings, "KeyedSettings", LookMode.Value, LookMode.Deep);
			base.ExposeData();
			if (Scribe.mode == LoadSaveMode.LoadingVars)
			{
				ValidateKeyedSettings();
			}
		}
		public void DoSettingsWindowContents(Rect inRect)
		{
			Rect leftRect = inRect.LeftHalf();
			Rect rightRect = inRect.RightHalf();

			//GameFont originalFont = Text.Font;
			//GameFont titleFont = GameFont.Medium;

			//left half
			Listing_Standard leftListingStandard = new Listing_Standard();
			leftListingStandard.Begin(leftRect);
			FeatureSettings(leftRect, leftListingStandard);
			leftListingStandard.End();

			//right half
			Listing_Standard rightListingStandard = new Listing_Standard();
			rightListingStandard.Begin(rightRect);
			ContentSettings(rightRect, rightListingStandard);
			DevSettingsContents(rightRect, rightListingStandard);
			rightListingStandard.End();
		}

		public void FeatureSettings(Rect inRect, Listing_Standard listingStandard)
		{
			Text.Font = GameFont.Medium;
			listingStandard.Label("Ab_Feature_Settings".Translate());
			listingStandard.GapLine();

			Text.Font = GameFont.Small;
			listingStandard.CheckboxLabeled("Ab_Enable_FavorCostImpact".Translate(), ref RoyalFavorImpact, "Ab_Enable_FavorCostImpact_Tip".Translate());
			EnableAlienRaceProstheticGraphics.DrawSetting(listingStandard);
			EnableGenesForHairColor.DrawSetting(listingStandard);
			//MonoColoredDemiFox.DrawSetting(listingStandard);
		}

		public void ContentSettings(Rect inRect, Listing_Standard listingStandard)
		{
			Text.Font = GameFont.Medium;
			listingStandard.Label("Ab_Content_Settings".Translate());
			listingStandard.GapLine();

			Text.Font = GameFont.Small;
			EnableProsthetics.DrawSetting(listingStandard);
			//ToDo: Write a few patches.
			//EnableRoyalFavorImpactPatches.DrawSetting(listingStandard);
			listingStandard.CheckboxLabeled("Ab_Load_ScenarioDefs".Translate(), ref CustomStartScenarios, "Ab_Load_ScenarioDefs_Tip".Translate());

			listingStandard.Gap(20f);
		}

		public void DevSettingsContents(Rect inRect, Listing_Standard listingStandard)
		{
			if (!Prefs.DevMode)
			{
				return;
			}
			Text.Font = GameFont.Medium;
			listingStandard.Label("Ab_Dev_Settings".Translate());
			listingStandard.GapLine();

			Text.Font = GameFont.Small;
			listingStandard.CheckboxLabeled("Ab_Enable_DeveloperContent".Translate(), ref DevAssets, "Ab_Enable_DeveloperContent_Tip".Translate());
			listingStandard.CheckboxLabeled("Ab_Enable_HarmonyLogging".Translate(), ref Harmony_Logging, "Ab_Enable_HarmonyLogging_Tip".Translate());
			listingStandard.CheckboxLabeled("Ab_Enable_AlienRaceLogging".Translate(), ref HAR_Logging, "Ab_Enable_AlienRaceLogging_Tip".Translate());
			listingStandard.CheckboxLabeled("Ab_Force_AlienRaceLogging".Translate(), ref HAR_ForceLogging, "Ab_Force_AlienRaceLogging_Tip".Translate());

			listingStandard.Gap(20f);
		}
	}

	public class Ab_Mod : Mod
	{
		public static Settings settings;
		public Ab_Mod(ModContentPack content) : base(content)
		{
			settings = GetSettings<Settings>();
			settings.ValidateKeyedSettings();
			Harmony harmony = new Harmony("AbUtility");
			harmony.Patch(AccessTools.Method(typeof(DirectXmlLoader), "DefFromNode"), new HarmonyMethod(typeof(UnloadDefs_Patch), "ShouldUnloadDef"));//We need this patch to run early
			harmony.Patch(AccessTools.Method(typeof(DefaultGraphicsLoader), "LoadAll2DVariantsForGraphic"), new HarmonyMethod(typeof(SilenceTheAlien_Patch), nameof(SilenceTheAlien_Patch.SetAlienRaceLogging)));
		}

		public override void DoSettingsWindowContents(Rect inRect)
		{
			settings.DoSettingsWindowContents(inRect);
		}

		public override string SettingsCategory()
		{
			return "Ab_Utility".Translate();
		}
	}
}