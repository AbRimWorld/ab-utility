﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public static class GenderUtility
	{
		public static bool Matches(this Gender gender, Gender otherGender)
		{
			if (gender == Gender.None || otherGender == Gender.None)
			{
				return true;
			}
			return gender == otherGender;
		}
	}
}