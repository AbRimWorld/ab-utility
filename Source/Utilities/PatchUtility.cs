﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ab_Utility
{
    public static class PatchUtility
    {
        public static MethodInfo GetFirstDelegateForMethod(this Type type, string name)
        {
            MethodInfo finalDelegate = null;
            AccessTools.FindIncludingInnerTypes(type, (Type t) =>
            {
                MethodInfo foundDelegate = t.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic)
                    .FirstOrDefault(mi => mi.Name.Contains(name));
                if (foundDelegate != null)
                {
                    finalDelegate = foundDelegate;
                    return t;
                }
                return null;
            });
            return finalDelegate;
        }
    }
}
