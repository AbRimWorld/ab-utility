﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public static class ToxFromTendUtility
	{
		public static bool CanToxFromTend(Pawn pawn)
		{
			if (pawn.genes == null)
			{
				return false;
			}
			if (!pawn.genes.GenesListForReading.Any(gene => gene.def.HasModExtension<GeneDefExtension_ToxicTendFlag>()))
			{
				return false;
			}
			return true;
		}

		public static float ToxBuildupToAdd(Pawn pawn)
		{
			float patientBodySize = pawn.BodySize;
			float resistance = pawn.GetStatValue(StatDefOf.ToxicResistance);
			FloatRange toxRange = new FloatRange(0.01f, 0.15f);
			return (toxRange.RandomInRange / patientBodySize) * (1-resistance);
		}
	}
}
