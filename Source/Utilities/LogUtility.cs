﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
    public static class LogUtility
    {
        private static HashSet<int> previouslySentLogs = new HashSet<int>();
        private const int previousHistoryCount = 500;

        public static void ErrorOnce(string message)
        {
            int hash = message.GetHashCode();

            if(previouslySentLogs.Contains(hash) )
            {
                return;
            }
            previouslySentLogs.Add(hash);
            if(previouslySentLogs.Count > previousHistoryCount )
            {
                previouslySentLogs.Remove(previouslySentLogs.First());
            }
            Log.Error(message);

        }
    }
}
