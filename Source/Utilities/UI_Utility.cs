﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public static class UI_Utility
	{
		// From NightmareCore 
		//ToDo : if it ever releases use it as a dependancy instead
		private const float DefaultSliderWidth = 16;
		#region Scroll View
		public static Rect CreateInnerScrollRect(Rect outerRect, float height, float sliderWidth = DefaultSliderWidth)
		{
			return new Rect
			(
				0,
				0,
				outerRect.width - sliderWidth,
				height
			);
		}
		public static void MakeAndBeginScrollView(Rect inRect, float requiredHeight, ref Vector2 scrollPosition, out Listing_Standard list)
		{
			MakeAndBeginScrollView(inRect, requiredHeight, ref scrollPosition, out Rect innerRect);
			list = new Listing_Standard()
			{
				ColumnWidth = innerRect.width,
				maxOneColumn = true
			};
			list.Begin(innerRect);
		}
		public static void MakeAndBeginScrollView(Rect inRect, float requiredHeight, ref Vector2 scrollPosition, out Rect outRect)
		{
			outRect = CreateInnerScrollRect(inRect, requiredHeight);
			Widgets.BeginScrollView(inRect, ref scrollPosition, outRect);
		}

		public static void EndScrollView(this Listing_Standard list, ref float requiredHeight)
		{
			requiredHeight = list.CurHeight;
			list.End();
			Widgets.EndScrollView();
		}
		#endregion
		/// <summary>
		/// Draw label with increased size
		/// </summary>
		/// <param name="list">Listing_Standard to create label for</param>
		/// <param name="label">Label to present</param>
		/// <param name="drawGapLine">Whether or not a line should be drawn after the header</param>
		public static void HeaderLabel(this Listing_Standard list, string label, bool drawGapLine = true)
		{
			GameFont originalFont = Text.Font;
			Text.Font = GameFont.Medium;
			list.Label(label);
			if (drawGapLine)
				list.GapLine();
			Text.Font = originalFont;
		}

		/// <summary>
		/// Create a new inner rectangle that is centered in respect to the input rectangle
		/// </summary>
		/// <param name="inRect"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="padding"></param>
		/// <returns></returns>
		public static Rect CreateCenteredInnerRect(Rect inRect, float width = 0, float height = 0, float padding = 0)
		{
			width = Mathf.Min(width, inRect.width);
			height = Mathf.Min(height, inRect.height);

			bool isTooTightWithPadding = width + 2 * padding > inRect.width
				|| height + 2 * padding > inRect.height;

			Rect paddedRect = isTooTightWithPadding ? inRect : inRect.ContractedBy(padding);

			if (width == 0 || width > paddedRect.width)
				width = paddedRect.width;
			if (height == 0 || height > paddedRect.height)
				height = paddedRect.height;
			float startX = paddedRect.x + ((paddedRect.width - width) / 2);
			float startY = paddedRect.y + ((paddedRect.height - height) / 2);
			return new Rect(startX, startY, width, height);
		}

		/// <summary>
		/// Align a given label to the center of the given Rect
		/// </summary>
		/// <param name="inRect">Input Rect</param>
		/// <param name="label">Label to align</param>
		/// <param name="labelRect">Produced Rect for Label</param>
		/// <param name="padding">Padding around the label</param>
		/// <param name="font">Optional font to use to draw label</param>
		public static void LabelInCenter(Rect inRect, string label, out Rect labelRect, float padding = 0, GameFont? font = null)
		{
			GameFont originalFont = Text.Font;
			if (font != null)
				Text.Font = font.Value;
			float width = Text.CalcSize(label).x;
			if (width > inRect.width)
				width = inRect.width;
			float height = Text.CalcHeight(label, width);
			labelRect = CreateCenteredInnerRect(inRect, width, height, padding);
			Widgets.Label(labelRect, label);
			Text.Font = originalFont;
		}

		public static bool IconWithLabelClicked(Rect inRect, string label, Texture2D icon, float iconScale = 1f, Color? iconColor = null, TextAnchor labelAlignment = TextAnchor.MiddleLeft)
		{
			Rect iconRect = inRect.LeftPartPixels(inRect.height);
			Rect labelRect = inRect.RightPartPixels(inRect.width - iconRect.width);
			Color previousColor = GUI.color;
			if (iconColor != null)
			{
				GUI.color = iconColor.Value;
			}
			Widgets.DrawTextureFitted(iconRect, icon, iconScale);
			GUI.color = previousColor;
			TextAnchor previousTextAnchor = Text.Anchor;
			Text.Anchor = labelAlignment;
			Widgets.Label(labelRect, label);
			Text.Anchor = previousTextAnchor;

			return Widgets.ButtonInvisible(inRect);
		}
	}
}