﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class HediffComp_IncreaseToxResourceCapacity : HediffComp
	{
		private HediffCompProperties_IncreaseToxResourceCapacity Props => (HediffCompProperties_IncreaseToxResourceCapacity)props;

		public Gene_Tox gene => Pawn.genes.GetFirstGeneOfType<Gene_Tox>();

		public override void CompPostPostAdd(DamageInfo? dinfo)
		{
			base.CompPostPostAdd(dinfo);
			if (gene == null)
			{
				return;
			}
			gene.SetMax(gene.Max + Props.offsetMax);
		}
		public override void CompPostPostRemoved()
		{
			base.CompPostPostRemoved();
			gene.SetMax(gene.Max - Props.offsetMax);
			//Log.Message($"gene max = {gene.Max}");
		}
	}

	public class HediffCompProperties_IncreaseToxResourceCapacity : HediffCompProperties
	{
		public float offsetMax = 0f;
		public HediffCompProperties_IncreaseToxResourceCapacity()
		{
			compClass = typeof(HediffComp_IncreaseToxResourceCapacity);
		}
	}
}