﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class HediffComp_MartyrdomImplants_TriggerOnDeath : HediffComp_MartyrdomImplants, ICorpseTickable
	{
		private HediffCompProperties_MartyrdomImplants_TriggerOnDeath Props => (HediffCompProperties_MartyrdomImplants_TriggerOnDeath)props;
#if v1_4
		public override void Notify_PawnDied()
		{
			base.Notify_PawnDied();
#else
		public override void Notify_PawnDied(DamageInfo? dinfo, Hediff culprit = null)
		{
			base.Notify_PawnDied(dinfo, culprit);
#endif
			if (!Pawn.Dead)
			{
				return;
			}
			SetDelayedEffectTick();
			Log.Message($"current tick: {GenTicks.TicksGame}, Target tick: {delayedEffectTick}");
		}

		public override void DoOutcome()
		{
			PlayVisuals();
			SpawnFilth();
			PlaySound();
			DestroyHediffOnTrigger();
			if (Props.resurrectOnTrigger)
			{
				Ressurect();
			}
			ApplyHediffGiver();
			SpawnExplosionOnSelf();
		}
		private void Ressurect()
		{
			if (!Props.resurrectOnTrigger)
			{
				return;
			}
#if v1_4
			ResurrectionUtility.ResurrectWithSideEffects(parent.pawn);
#else
			ResurrectionUtility.TryResurrectWithSideEffects(parent.pawn);
#endif
		}

		public void CorpseTickRare()
		{
			TriggerOutcome();
		}
	}
	public class HediffCompProperties_MartyrdomImplants_TriggerOnDeath : HediffCompProperties_MartyrdomImplants
	{
		public bool resurrectOnTrigger = false;

		public HediffCompProperties_MartyrdomImplants_TriggerOnDeath()
		{
			compClass = typeof(HediffComp_MartyrdomImplants_TriggerOnDeath);
		}
	}
}