﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace Ab_Utility
{
	public class HediffComp_MartyrdomImplants : HediffComp
	{
		public const int DefaultDelayedEffect = -1;
		protected int delayedEffectTick = DefaultDelayedEffect;
		private HediffCompProperties_MartyrdomImplants Props => (HediffCompProperties_MartyrdomImplants)props;
		public override void CompPostTick(ref float severityAdjustment)
		{
			base.CompPostTick(ref severityAdjustment);
			TriggerOutcome();
		}
		
		public void TriggerOutcome()
		{
			if (delayedEffectTick == DefaultDelayedEffect)
			{
				return;
			}
			if (GenTicks.TicksGame < delayedEffectTick)
			{
				return;
			}
			DoOutcome();
			delayedEffectTick = DefaultDelayedEffect;
		}
		public void SetDelayedEffectTick()
		{
			if (delayedEffectTick == DefaultDelayedEffect)
			{
				delayedEffectTick = GenTicks.TicksGame + Props.tickDelayTriggerEffectRange.RandomInRange;
			}
		}

		public void DestroyHediffOnTrigger()
		{
			if (Props.destroyHediffOnTrigger)
			{
				parent.pawn.health.RemoveHediff(parent);
			}
		}

		public void CastAbilitiesOnSelf()// ToDo: remove 
		{
			foreach (Ability ability in Props.abilitiesToCast)
			{
				// cast on pawn (parent)
			}
		}
		public void IngestThing()
		{
			foreach (ThingDef chemical in Props.thingsToIngest)
			{
				Pawn.Ingested(Pawn, chemical.ingestible.CachedNutrition);//doesn't work on downed pawns
			}
		}
		public void SpawnExplosionOnSelf()
		{
			foreach (Explosion explosion in Props.explosions)
			{
				//GenExplosion.DoExplosion(Pawn.Position, Pawn.MapHeld, explosion.damType, Pawn);
			}
		}
		public void ApplyHediffGiver()
		{
			foreach (HediffGiver def in Props.applyHediffGiver)
			{
				def.TryApply(Pawn);
			}
		}

		//Visuals & Filth & Sound
		public void PlayVisuals()
		{
			if (Props.mote == null && Props.fleck == null)
			{
				return;
			}
			Vector3 drawPos = base.Pawn.DrawPos;
			for (int i = 0; i < Props.moteCount; i++)
			{
				Vector2 vector = Rand.InsideUnitCircle * Props.moteOffsetRange.RandomInRange * Rand.Sign;
				Vector3 loc = new Vector3(drawPos.x + vector.x, drawPos.y, drawPos.z + vector.y);
				if (Props.mote != null)
				{
					MoteMaker.MakeStaticMote(loc, base.Pawn.Map, Props.mote);
				}
				else
				{
					FleckMaker.Static(loc, base.Pawn.Map, Props.fleck);
				}
			}
		}
		public void SpawnFilth()
		{
			if (Props.filth == null)
			{
				return;
			}
			FilthMaker.TryMakeFilth(base.Pawn.Position, base.Pawn.Map, Props.filth, Props.filthCount);
		}
		public void PlaySound()
		{
			if (Props.sound == null)
			{
				return;
			}
			Props.sound.PlayOneShot(SoundInfo.InMap(base.Pawn));
		}
		public virtual void DoOutcome()
		{

		}

		public override void CompExposeData()
		{
			base.CompExposeData();
			Scribe_Values.Look(ref delayedEffectTick, nameof(delayedEffectTick));
		}
	}

	public class HediffCompProperties_MartyrdomImplants : HediffCompProperties
	{
		public bool destroyHediffOnTrigger = false;
		public IntRange tickDelayTriggerEffectRange = new IntRange(0, 0); // in xml 0~0

		public FleckDef fleck = null;
		public ThingDef mote = null;
		public int moteCount = 3;
		public FloatRange moteOffsetRange = new FloatRange(0.2f, 0.4f);
		public ThingDef filth;
		public int filthCount = 4;

		public List<Ability> abilitiesToCast = new List<Ability>();
		public List<ThingDef> thingsToIngest = new List<ThingDef>();
		public List<Explosion> explosions = new List<Explosion>();
		public List<HediffGiver_Instant> applyHediffGiver = new List<HediffGiver_Instant>();
		//public List<>;// effects such as reserrection

		public SoundDef sound = null;

		public HediffCompProperties_MartyrdomImplants()
		{
			compClass = typeof(HediffComp_MartyrdomImplants);
		}
	}
}