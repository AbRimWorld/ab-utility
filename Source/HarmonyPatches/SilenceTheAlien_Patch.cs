﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using static AlienRace.AlienPartGenerator;

namespace Ab_Utility
{
	//[HarmonyPatch(typeof(ExtendedGraphicTop), "Debug")]
	//[HarmonyPatch(MethodType.Getter)]
	public static class SilenceTheAlien_Patch
	{
		public static void SetAlienRaceLogging(ref bool shouldLog)
		{
			if (Ab_Mod.settings.HAR_Logging == false)
			{
				shouldLog = Ab_Mod.settings.HAR_Logging;
			}
			if (Ab_Mod.settings.HAR_ForceLogging == true && Ab_Mod.settings.HAR_Logging == true)
			{
				shouldLog = Ab_Mod.settings.HAR_ForceLogging;
			}
		}
	}
}