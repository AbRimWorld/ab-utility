﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[HarmonyPatch(typeof(TendUtility), nameof(TendUtility.CalculateBaseTendQuality), new Type[] {typeof(Pawn), typeof(Pawn), typeof(ThingDef)})]
	public static class TendUtility_Patch
	{
		[HarmonyPostfix]
		public static void ToxFromTend(Pawn doctor, Pawn patient, ThingDef medicine)
		{
			if (patient.Map == null)
			{
				return;
			}
			bool shouldTox = ToxFromTendUtility.CanToxFromTend(doctor);
			if (!shouldTox)
			{
				return;
			}
			float toxValue = ToxFromTendUtility.ToxBuildupToAdd(patient);
			HealthUtility.AdjustSeverity(patient, HediffDefOf.ToxicBuildup, toxValue);
		}
	}
}