﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;
using HarmonyLib;

namespace Ab_Utility
{

    [HarmonyPatch(typeof(Corpse), nameof(Corpse.TickRare))]
    public static class Corpse_Patch
    {
        [HarmonyPostfix]
        public static void CallHediffCompsWithCorpseTickable(ref Corpse __instance)
        {
            try
            {
                List<ICorpseTickable> corpseTickables = __instance.InnerPawn?.health?.hediffSet?.hediffs?
                    .Where(hed => hed is HediffWithComps)?
                    .Cast<HediffWithComps>()?
                    .SelectMany(hed => hed.comps)?
                    .Where(comp => comp is ICorpseTickable)?
                    .Cast<ICorpseTickable>()?
                    .ToList();
                if (corpseTickables.NullOrEmpty())
                {
                    return;
                }
                foreach (ICorpseTickable corpseTickable in corpseTickables)
                {
                    corpseTickable.CorpseTickRare();
                }
            }
            catch(Exception e)
            {
                Log.Error($"Caught unhandled exception: {e}");
            }
        }
    }
}
