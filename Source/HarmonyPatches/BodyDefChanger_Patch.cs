﻿using AlienRace;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[HarmonyPatch(typeof(HarmonyPatches), "ReplacedBody")]
	public static class BodyDefChanger_Patch
	{
		private static bool isPatchDisabled = false;
		[HarmonyPostfix]
		private static void SwapBodyDef(Pawn pawn, ref BodyDef __result)
		{
			try 
			{
				if (isPatchDisabled)
				{
					isPatchDisabled = false;
					return;
				}
				//bool hasStageBody = StageBodyUtility.HasHediffStageBodies(pawn);
				//if (!hasStageBody)
				//{
				//	return;
				//}
				BodyDef body = GetHediffStageBody(pawn);
				if (body != null)
				{
					__result = body;
					return;
				}
			}
			catch (Exception e)
			{
				string message = "something went wrong while trying to change bodyDef \n" + e;
				Log.ErrorOnce(message,message.GetHashCode());
			}
		}
		private static Dictionary<Pawn, BodyDef> cachedPawnBodies = new Dictionary<Pawn, BodyDef>();
		private static BodyDef GetHediffStageBody(Pawn pawn)
		{
			StageBody currentBody = StageBodyUtility.GetActiveStageBody(pawn);
			BodyDef currentBodyDef = null;
			if (currentBody != null)
			{
				currentBodyDef = currentBody.bodyDef;
			}
			else
			{
				isPatchDisabled = true;
				currentBodyDef = HarmonyPatches.ReplacedBody(pawn);
			}
			
			BodyDef cachedBodyDef = cachedPawnBodies.TryGetValue(pawn, null);
			cachedPawnBodies.SetOrAdd(pawn, currentBodyDef);

			if (currentBodyDef != cachedBodyDef)
			{
				//Log.Message(pawn + "bodyDef changed from " + cachedBodyDef + "to" + currentBodyDef);
				BodyHasChanged(pawn, cachedBodyDef, currentBodyDef);
			}
			return currentBodyDef;
		}

		private static void BodyHasChanged(Pawn pawn, BodyDef oldBodyDef, BodyDef newBodyDef)
		{
			if (newBodyDef == null || oldBodyDef == null)
			{
				return;
			}
			if (newBodyDef == oldBodyDef)
			{
				return;
			}
			
			ChangeHediffs(pawn, newBodyDef);
		}
		private static void ChangeHediffs(Pawn pawn, BodyDef bodyDef)
		{
			List<Hediff> hediffs = pawn.health.hediffSet.hediffs;
			foreach (Hediff hediff in hediffs.ToList())
			{
				BodyPartDef partDef = hediff.Part?.def;
				string label = hediff.Part?.Label;
				if (partDef == null || label == null)
				{
					//Log.Message("part or label is null");
					continue;
				}
				BodyPartRecord newPart = bodyDef.AllParts.FirstOrDefault(part => part.def == partDef && part.Label == label);
				if (newPart != null)
				{
					hediff.Part = newPart;
					//Log.Message("moved hediff to new bodyPart");
					continue;
				}
				ThingDef thingToSpawn = hediff.def.spawnThingOnRemoved;
				if (thingToSpawn != null)
				{
					GenSpawn.Spawn(thingToSpawn, pawn.Position, pawn.Map);
					//Log.Message("spawned thingDef");
					continue;
				}
				//Log.Message("all else failed remove hediff");
				pawn.health.RemoveHediff(hediff);
			}
		}

		public static BodyDef GetFullStackBody(Pawn pawn)
		{
			return GetHediffStageBody(pawn) // if no hediff stage body was found, fall back to HAR body retrieval
				?? AlienRace.HarmonyPatches.ReplacedBody(pawn);

		}
	}
}