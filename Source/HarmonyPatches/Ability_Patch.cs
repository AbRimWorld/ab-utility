﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[HarmonyPatch(typeof(Ability), nameof(Ability.FinalPsyfocusCost))]
	public static class Ability_Patch
	{
		private static StatDef psyfocusStat = DefDatabase<StatDef>.GetNamed("Ab_PsyfocusCost");
		[HarmonyPostfix]
		public static void AdjustCost(ref float __result, Pawn ___pawn)
		{
			__result *= ___pawn.GetStatValue(psyfocusStat);
		}
	}
}