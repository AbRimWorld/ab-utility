﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public class Gene_Tox : Gene_Resource, IGeneResourceDrain
	{
		const float toxValuePerGas = 0.01f;//1 / 45;
		public Gene_Resource Resource => this;

		public Pawn Pawn => pawn;

		public bool CanOffset => true;

		public string DisplayLabel => Label + $" ({"Gene".Translate()})";

		public float ResourceLossPerDay => def.resourceLossPerDay;

		public override float InitialResourceMax => 1f;

		public override float MinLevelForAlert => -1f;

		public override float MaxLevelOffset => 0.1f;

		public int AvailableGas => Mathf.FloorToInt(cur / toxValuePerGas);

		protected override Color BarColor => new ColorInt(29, 161, 36).ToColor; //0.7, 1.0, 0.7 is too bright

		protected override Color BarHighlightColor => new ColorInt(200, 255, 200).ToColor;

		public override void PostAdd()
		{
			if (ModLister.CheckBiotech("Genes"))
			{
				base.PostAdd();
				Reset();
				cur = 0f;
			}
		}

		public override void Tick()
		{
			base.Tick();
			GeneResourceDrainUtility.TickResourceDrain(this);
		}

		public override IEnumerable<Gizmo> GetGizmos()
		{
			foreach (Gizmo gizmo in base.GetGizmos())
			{
				yield return gizmo;
			}
			foreach (Gizmo resourceDrainGizmo in GeneResourceDrainUtility.GetResourceDrainGizmos(this))
			{
				yield return resourceDrainGizmo;
			}
		}

		public override void ExposeData()
		{
			base.ExposeData();
		}

		public void ConsumeGas(int amount)
		{
			cur -= amount * toxValuePerGas;
			cur = Mathf.Max(cur, 0);
		}
#if v1_4
		public override void Notify_PawnDied()
		{
			base.Notify_PawnDied();
#else
		public override void Notify_PawnDied(DamageInfo? dinfo, Hediff culprit = null)
		{
			base.Notify_PawnDied(dinfo, culprit);
#endif
			GenExplosion.DoExplosion(pawn.Position, pawn.Map, 5, DamageDefOf.ToxGas, pawn);
			AbilityComp_ReleaseToxGas.SpawnGas(this, int.MaxValue);
		}
	}
}