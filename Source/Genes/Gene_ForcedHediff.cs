﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class Gene_ForcedHediff : Gene
	{
		GeneDefExtension_ForcedHediff Extension => def.GetModExtension<GeneDefExtension_ForcedHediff>();
		List<Hediff> addedHediffs = new List<Hediff>();
		public override void PostAdd()
		{
			if (Extension == null)
			{
				throw new Exception($"{nameof(GeneDefExtension_ForcedHediff)} extension not found");
			}
			base.PostAdd();
			foreach (HediffGiver def in Extension.hediffGivers)
			{
				def.TryApply(pawn, addedHediffs);
			}
		}

		public override void PostRemove()
		{
			base.PostRemove();
			if (!Extension.removeHediffsOnGeneRemoval)
			{
				return;
			}
			foreach (Hediff hediff in addedHediffs)
			{
				if (pawn.health.hediffSet.hediffs.Contains(hediff))
				{
					pawn.health.RemoveHediff(hediff);
				}
			}
		}

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Collections.Look(ref addedHediffs, nameof(addedHediffs), LookMode.Reference);
		}
	}
}