﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class AbilityComp_SoulDrain : CompAbilityEffect
	{
		Pawn CasterPawn => base.parent.pawn;
		AbilityCompProperties_SoulDrain DrainProps => base.Props as AbilityCompProperties_SoulDrain;

		public override void Apply(LocalTargetInfo target, LocalTargetInfo dest)
		{
			base.Apply(target, dest);
			if (target.Thing is Plant targetPlant)
			{
				Action extraSelectionAction = () => DrainAnimaTree(targetPlant);
				new AbilityInteractionSelector_Window(parent, CasterPawn, extraSelectionAction);
			}
			else if (target.Thing is Pawn targetPawn)
			{
				DrainPawn(targetPawn); // not possible for now
			}
		}

		public override bool CanApplyOn(LocalTargetInfo target, LocalTargetInfo dest)
		{
			if (target.Thing is Plant targetPlant)
			{
				CompSpawnSubplant comp = targetPlant.TryGetComp<CompSpawnSubplant>();
				if (comp == null)
				{
					return false;
				}
				return true;
			}
			else if (target.Thing is Pawn targetPawn)
			{
				bool? canDrainPawn = CasterPawn.def.GetModExtension<ThingDefExtension_SoulDrainer>()?.SoulDrainerFor(CasterPawn)?.CanDrain(CasterPawn, targetPawn);
				if (canDrainPawn.HasValue && canDrainPawn.Value == false)
				{
					return false;
				}
				return true;
			}
			return false;
		}
		private void DrainAnimaTree(Thing drainTarget)
		{
			CompSpawnSubplant comp = drainTarget.TryGetComp<CompSpawnSubplant>();
			int animaGrassCount = comp.SubplantsForReading.Count;
			if (animaGrassCount >= DrainProps.minGrassCount)
			{
				IEnumerable<Thing> grass = comp.SubplantsForReading.Take(DrainProps.minGrassCount);
				foreach (Thing thing in grass)
				{
					thing.Destroy();
				}
				comp.Cleanup();//resync active grass count.
			}
			else
			{
				IntVec3 postision = drainTarget.Position;
				Map map = drainTarget.Map;
				drainTarget.Destroy();
				GenSpawn.Spawn(Ab_ThingDefOf.BurnedTree, postision, map);

				CasterPawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(Common.AnimaScream);
			}
		}
		private void DrainPawn(Pawn targetPawn)
		{
			HealthUtility.AdjustSeverity(targetPawn, Common.Ab_DrainDebuff, 0.3f);
			HealthUtility.AdjustSeverity(CasterPawn, Common.Ab_DrainBuff, 0.5f);
		}
	}
	public class AbilityCompProperties_SoulDrain : CompProperties_AbilityEffect
	{
		public int minGrassCount = 5;
		// ToDo : make the Buff and DeBuff XML defined.
		//public Hediff buff = null;
		//public Hediff debuff = null;

		public AbilityCompProperties_SoulDrain()
		{
			compClass = typeof(AbilityComp_SoulDrain);
		}
	}

	
}
