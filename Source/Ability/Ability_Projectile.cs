﻿using RimWorld;
using System;
using Verse;

namespace Ab_Utility
{
	public class Ability_Projectile : Ability
	{
		public Ability_Projectile() : base() {}

		public Ability_Projectile(Pawn pawn) : base(pawn) {}

		public Ability_Projectile(Pawn pawn, Precept sourcePrecept) : base(pawn, sourcePrecept) {}

		public Ability_Projectile(Pawn pawn, AbilityDef def) : base(pawn, def) {}

		public Ability_Projectile(Pawn pawn, Precept sourcePrecept, AbilityDef def) : base(pawn, sourcePrecept, def) {}

		public override bool Activate(LocalTargetInfo target, LocalTargetInfo dest)
		{
			if (!this.def.HasModExtension<Extension_Projectile>())
			{
				return base.Activate(target, dest);
			}
			Projectile projectile = GenSpawn.Spawn(this.def.GetModExtension<Extension_Projectile>().projectile, this.pawn.Position, this.pawn.Map) as Projectile;
			if (projectile == null)
			{
				Log.Message("No projectile");
				return false;
			}
			projectile.Launch(this.pawn, this.pawn.DrawPos, target, target, ProjectileHitFlags.IntendedTarget);
			return true;
		}
	}
	public class Extension_Projectile : DefModExtension
	{
		public ThingDef projectile;
	}

}
