﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public class AbilityCompProperties_ReleaseToxGas : AbilityCompProperties
	{
		public AbilityCompProperties_ReleaseToxGas()
{
			compClass = typeof(AbilityComp_ReleaseToxGas);
		}
	}

	public class AbilityComp_ReleaseToxGas : AbilityComp
	{
		IntRange gasSpawnAmountRange = new IntRange(2, 3);
		private const int GasSpawnFactor = 50;
		private const int GasSpawninterval = 10;
		bool shouldSpawnGas = false;
		Gene_Tox gene = null;
		public override void CompTick()
		{
			base.CompTick();
			//Log.Message($"{parent.pawn.LabelShort} Compt Tick start.");
			if (this.parent.Casting)
			{
				//Log.Message($"{parent.pawn.LabelShort} is casting.");
				shouldSpawnGas = true;
			}
			if (shouldSpawnGas)
			{
				//Log.Message($"{parent.pawn.LabelShort} spawning gas.");
				SpawnGas();
			}
		}
		public override void Initialize(AbilityCompProperties props)
		{
			base.Initialize(props);
			if (parent.pawn.genes == null)
			{
				return;
			}
			//Log.Message($"Initialize Ability {parent.pawn.LabelShort} not null");
			if (!parent.pawn.genes.HasGene(Ab_GeneDefOf.Ab_ToxGene))
			{
				return;
			}
			//Log.Message($"Gene : {GeneDefOf.Ab_ToxGene.label}");
			//Log.Message($"Initialize Ability {parent.pawn.LabelShort} has gene");
			gene = parent.pawn.genes.GetFirstGeneOfType<Gene_Tox>();
		}
		public bool CanSpawnGas
		{
			get
			{
				if (gene == null)
				{
					return false;
				}
				//Log.Message($"CanSpawnGas {parent.pawn.LabelShort} has gene");
				if (gene.AvailableGas <= 0)
				{
					return false;
				}
				//Log.Message($"CanSpawnGas {parent.pawn.LabelShort} Enough gas");
				return true;
			}
		}

		public override bool GizmoDisabled(out string reason)
		{
			if (!base.GizmoDisabled(out reason))
			{
				return false;
			}
			if (!CanSpawnGas)
			{
				reason = "not enough gas";
				return false;
			}
			return true;
		}

		public override void PostExposeData()
		{
			base.PostExposeData();
			Scribe_References.Look(ref gene, nameof(gene));
			Scribe_Values.Look(ref shouldSpawnGas, nameof(shouldSpawnGas));
		}
		private void SpawnGas()
		{
			//Log.Message($"{parent.pawn.LabelShort}: Start spawn gas.");
			if (gene.AvailableGas <= 0)
			{
				//Log.Message($"{parent.pawn.LabelShort} AvailableGas: {gene.AvailableGas} is too little.");
				shouldSpawnGas = false;
				return;
			}
			//Log.Message($"{parent.pawn.LabelShort} AvailableGas: {gene.AvailableGas} is enough, checking hash.");
			if (!parent.pawn.IsHashIntervalTick(GasSpawninterval))
			{
				//Log.Message($"{parent.pawn.LabelShort} IsHashIntervalTick is false");
				return;
			}
			//Log.Message($"=============={parent.pawn.LabelShort} AvailableGas: {gene.AvailableGas} is enough.=======================");
			int gasToSpawn = gasSpawnAmountRange.RandomInRange;
			SpawnGas(gene, gasToSpawn);
		}
		public static void SpawnGas(Gene_Tox gene, int gasToSpawn)
		{
			gasToSpawn = Mathf.Min(gasToSpawn, gene.AvailableGas);
			GasUtility.AddGas(gene.pawn.Position, gene.pawn.MapHeld, GasType.ToxGas, gasToSpawn * GasSpawnFactor);
			gene.ConsumeGas(gasToSpawn);
		}
	}
}
