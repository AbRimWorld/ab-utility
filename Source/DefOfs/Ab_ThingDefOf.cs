﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[DefOf]
	public static class Ab_ThingDefOf
	{
		public static ThingDef BurnedTree;

		static Ab_ThingDefOf()
		{
			DefOfHelper.EnsureInitializedInCtor(typeof(Ab_ThingDefOf));
		}
	}
}