﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class GeneDefExtension_ForcedHediff : DefModExtension
	{
		public bool removeHediffsOnGeneRemoval;
		public List<HediffGiver> hediffGivers = new List<HediffGiver>();

		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string error in base.ConfigErrors())
			{
				yield return error;
			}
			   
			if (hediffGivers.NullOrEmpty())
			{
				yield return $"HediffGiver \"{nameof(hediffGivers)}\" is null or empty";
			}
		}
	}
}
