﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class ThingDefExtension_ForcedGenes : DefModExtension
	{
		public float chance = 1.0f;// to even trigger anything.
		private List<Pair<List<GeneDef>, float>> forcedGenes = new List<Pair<List<GeneDef>, float>>();

		public List<GeneDef> GetRandomGenes()
		{
			if (!Rand.Chance(chance))
			{
				return new List<GeneDef>();
			}
			return forcedGenes.RandomElementByWeight(entry => entry.Second).First;
		}
		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string error in base.ConfigErrors())
			{
				yield return error;
			}
			if (chance > 1.0f)
			{
				chance = 1.0f;
			}
			if (chance <= 0)
			{
				yield return ("Chance to trigger Extension_ForcedGenes is 0% or less");
			}
		}
	}
}