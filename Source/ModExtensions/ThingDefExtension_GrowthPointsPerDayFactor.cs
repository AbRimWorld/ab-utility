﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class ThingDefExtension_GrowthPointsPerDayFactor : DefModExtension
	{
		public float growthPointsPerDayFactor = 1.0f;
		public float growthPointsPerDayVatFactor = 1.0f;

		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string error in base.ConfigErrors())
			{
				yield return error;
			}
		}
	}
}
