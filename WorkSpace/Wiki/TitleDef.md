# TitleValidator

## Conditions

## Logic Gate Validators

### TitleValidator_AlwaysValid
this is the default gate and always returns **true**.

### TitleValidator_Or
An **OR** gate, where if either `TitleValidator` returns **true** the whole gat returns **true**.
```xml
<modExtensions>
	<li Class="Ab_Utility.RoyalTitleDefExtension_SpecialTitle">
		<validator Class="Ab_Utility.TitleValidator_Or">
			<firstValidator Class="NameSpace.ExtensionClassCondition1" />
			<secondValidator Class="NameSpace.ExtensionClassCondition2">
				<xml>
					<li>item1</li>
					<li>item2</li>
				</xml>
			</secondValidator>
		</validator>
	</li>
</modExtensions>
```
`<firstValidator>` and `<secondValidator>` can take in any validators which returns a **bool**.

### TitleValidator_And
An **AND** gate, returns **true** only when both validators return **true**.
```xml
<modExtensions>
	<li Class="Ab_Utility.RoyalTitleDefExtension_SpecialTitle">
		<validator Class="Ab_Utility.TitleValidator_And">
			<firstValidator Class="NameSpace.ExtensionClassCondition1" />
			<secondValidator Class="NameSpace.ExtensionClassCondition2">
				<xml>
					<li>item1</li>
					<li>item2</li>
				</xml>
			</secondValidator>
		</validator>
	</li>
</modExtensions>
```
`<firstValidator>` and `<secondValidator>` can take in any validators which returns a **bool**.

### TitleValidator_Invert
Inverts the **bool** output of any validator.
```xml
<modExtensions>
	<li Class="Ab_Utility.RoyalTitleDefExtension_SpecialTitle">
		<validator Class="Ab_Utility.TitleValidator_Invert">
			<validator Class="NameSpace.ExtensionClassCondition" />
		</validator>
	</li>
</modExtensions>
```
`<validator>` takes any validator which returns a **bool** 