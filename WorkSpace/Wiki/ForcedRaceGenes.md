# Extension_ForcedGenes
To be placed as a ModExtension on a Race `<ThingDef>`.

Adds a list of Genes onto a pawn by weigh so long as they are not Baby or Newborn developmental stages.

This is to prevent children from being forced genes instead of inheriting them.
```xml
		<modExtensions>
			<li Class="Ab_Utility.ThingDefExtension_ForcedGenes" MayRequire="Ludeon.RimWorld.Biotech">
				<chance>0.2</chance>
				<forcedGenes>
					<li>
						<first>
							<li>Gene1</li>
						</first>
						<second>5</second>
					</li>
					<li>
						<first>
							<li>Gene2</li>
						</first>
						<second>2</second>
					</li>
					<li>
						<first>
							<li>Gene1</li>
							<li>Gene2</li>
						</first>
						<second>1</second>
					</li>
				</forcedGenes>
			</li>
		</modExtensions>
	</ThingDef>
```
`<chance>` is a control on how frequantly adding any of these genes is even attempted. Default value is `1.0`(100%) and is the value which will be used if `<chance>` is omitted.

`<li>` are alternative entries, only one will be selected as the result.

`<first>` is a list of GeneDefs, place as many as you need.

`<second>` is the weight, the higher this value is the higher the chane that entry is picked.