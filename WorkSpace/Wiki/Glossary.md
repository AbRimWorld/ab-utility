Should a user not understand the Wiki terms.

## Defs
The game reads data from Defs which it uses to construct and populate it's database, this gives you all your items, races, quests, and almost evyerthing you can interact with. Most of the things you see in your game are defined in Defs.

RimWorld also stores all it's Defs within a **Defs** folder.

## Def Types
Rimworld has numerious Types of Defs and they can easily found with the fact they are the direct child node of `<Defs>` and also have their own children nodes.

Def Types can also have simbling nodes of the same Type unlike their children with the exception of `<li>`.

Example:
```xml
<Defs>
	<DefType>
		<defName>ThisIsAnExampleOfADef</defName>
	</DefType>
</Defs>
```
The `DefType` above can be substituted with any of the valid Def Types defined via C#.

Some examples of Def Types used in RimWorld are:
- `<AbilityDef>`
- `<AbilityCategoryDef>`
- `<BiomeDef>`
- `<CultureDef>`
- `<DamageDef>`
- `<ThingDef>`
- `<HediffDef>`
- `<InteractionDef>`
- `More...`