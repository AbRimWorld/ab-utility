# Martyr Implants
Trigger on demand or on down

Effect can affect the caster / host

Hediff / Ability requires a loaded and exhausted state.

?Trigger all Ability Hediffs or specific ?

<br />
<br />

# Throne Binding
Gain abilities of great power by trapping a Psycaster into a building (Their throne)
Some for of tracked upgrade to allow this feature
Add linkable buildings to amplify the power of the EnTombed pawn

<br />
<br />

# Worn Turrets
Wearing certain apparel adds turrets to the wearer.

<br />
<br />

# Omega Gear

<br />
<br />

# Venerated Dead Meme
A meme forcing precepts where pawns prepare for their death expecting either beautiful or wealthy final resting places. 
<br />
<br />

# Aesthetic Shaper 
Allows the User pawn to input configuration and select a bodyType of their choice.

Perhapse also allow adding xenoGenes such as furskin for example.

This can also allow the selection of Noses for the Aesthetic Nose Hediff.

Requires the ability to remove certain cosmetic genes.