[h1]About the mod :[/h1]
A mod purely made to make my life easier by containing content and features to be used by my other mods.
This mod houses C#, assets, and mod settings I will be using across my own mods.

[h1]Known / Possible Issues :[/h1]
[list]
[*]Other mods forcing bodyDef changes.
-I don't know of any that do this apart from Humanoid Alien Races.
[/list]

[h1]Features :[/h1]
[list]
[*]bodyDef swapping via hediff stage.
[*]quest nodes which accept pawn variety.
[*]XML unloading.
[*]XPath with Boolean mod config support.
[/list]


[h1]Notes :[/h1]
This mod does alter Vanilla XML and Functions, but attempts to keep the result the same as vanilla.
You should make use of this mod's settings menu.
This mod is far from done and I will add more to it as I need it.
This mod can also be used to control Humanoid Alien Races logging while in Debug mode (see settings).

[h1]Credit :[/h1]
[list]
[*]Abraxas - Code and XML
[*][url=https://steamcommunity.com/id/NightmareCorporation/myworkshopfiles/?appid=294100]NightmareCorporation[/url] - Code and support
[*][url=https://steamcommunity.com/profiles/76561198182079259/myworkshopfiles/?appid=294100]ShauaPuta[/url] - Art
[*][url=https://steamcommunity.com/profiles/76561198057157623/myworkshopfiles/?appid=294100]VanillaSky[/url] - FacialAnimations earless texture
[*][url=https://steamcommunity.com/profiles/76561197992216829/myworkshopfiles/?appid=294100]Gouda quiche[/url] - Preview art
[/list]


[h1]Links :[/h1]
[url=https://gitlab.com/AbRimWorld/Ab-Utility]Git[/url]

[h3]Like my work and want to support me?[/h3]
　　　　　　　　　　　[url=https://ko-fi.com/abraxas_][img]https://cdn.ko-fi.com/cdn/kofi5.png[/img][/url]